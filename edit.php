<?php
$host = 'localhost';
$username = 'root';
$password = '';
$database = 'p8_exercise_backend';

$conn = new mysqli($host, $username, $password, $database);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

if ($_SERVER["REQUEST_METHOD"] === "GET" && isset($_GET["id"])) {
    $id = $_GET["id"];

    // Retrieve the employee information
    $sql = "SELECT * FROM employee WHERE id = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("i", $id);
    $stmt->execute();
    $result = $stmt->get_result();

    if ($result->num_rows == 1) {
        $row = $result->fetch_assoc();
        $first_name = $row["first_name"];
        $last_name = $row["last_name"];
        $middle_name = $row["middle_name"];
        $birthday = $row["birthday"];
        $address = $row["address"];
    } else {
        echo "Employee not found.";
        exit;
    }
} else {
    echo "Invalid request.";
    exit;
}
?>

<!DOCTYPE html>
<html>

<head>
    <title>Edit Employee</title>
</head>

<body>
    <h1>Edit Employee</h1>

    <form method="POST" action="update.php">
        <input type="hidden" name="id" value="<?php echo $id; ?>">
        <input type="text" name="first_name" placeholder="First Name" value="<?php echo $first_name; ?>" required>
        <input type="text" name="last_name" placeholder="Last Name" value="<?php echo $last_name; ?>" required>
        <input type="text" name="middle_name" placeholder="Middle Name" value="<?php echo $middle_name; ?>" required>
        <input type="text" name="birthday" placeholder="Birthday" value="<?php echo $birthday; ?>" required>
        <input type="text" name="address" placeholder="Address" value="<?php echo $address; ?>" required>
        <button type="submit">Update</button>
    </form>
</body>

</html>