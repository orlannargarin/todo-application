<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        .error {
            color: #FF0000;
        }
    </style>
    <title>Welcome</title>
</head>

<body>
    <?php
    $nameErr = $name = $msg = "";
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $name = $_POST['name'];

        // output personalized greeting message
        $name = test_input($_POST["name"]);

        // check if name only contains letters and whitespace
        if (!preg_match("/^[a-zA-Z-' ]*$/", $name)) {
            $nameErr = "Only letters and white space allowed";
        } else {
            $msg = "Hello " . $name . "! Welcome to our website.";
        }
    }
    function test_input($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    ?>

    <form method="POST">
        <label for="name">Enter your name:</label>
        <input type="text" id="name" name="name" required value="<?php echo $name; ?>">
        <span class="error">*
            <?php echo $nameErr; ?>
        </span>
        <br><br>
        <button type="submit">Submit</button>
    </form>

    <h2>
        <?php echo $msg ?>
    </h2>
</body>

</html>