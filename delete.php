<?php
$host = 'localhost';
$username = 'root';
$password = '';
$database = 'p8_exercise_backend';

$conn = new mysqli($host, $username, $password, $database);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

if ($_SERVER["REQUEST_METHOD"] === "GET" && isset($_GET["id"])) {
    $id = $_GET["id"];

    // Delete the employee from the database
    $sql = "DELETE FROM employee WHERE id=?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("i", $id);

    if ($stmt->execute()) {
        header("Location: exercise7.php"); // Redirect to the employee list page
    } else {
        echo "Error deleting employee: " . $stmt->error;
    }
} else {
    echo "Invalid request.";
}

?>