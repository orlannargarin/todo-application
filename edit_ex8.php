<?php
require_once('C:\xampp\htdocs\Internship\todo-application\vendor\thingengineer\mysqli-database-class\MysqliDb.php');
$host = 'localhost';
$username = 'root';
$password = '';
$database = 'p8_exercise_backend';

$db = new MysqliDb($host, $username, $password, $database);

if ($_SERVER["REQUEST_METHOD"] === "GET" && isset($_GET["id"])) {
    $id = $_GET["id"];

    // Retrieve the employee information
    $db->where('id', $id);
    $employee = $db->getOne('employee');

    if ($db->count == 1) {
        $first_name = $employee["first_name"];
        $last_name = $employee["last_name"];
        $middle_name = $employee["middle_name"];
        $birthday = $employee["birthday"];
        $address = $employee["address"];
    } else {
        echo "Employee not found.";
        exit;
    }

    // Close the database connection
    $db->disconnect();
} else {
    echo "Invalid request.";
    exit;
}
?>

<!DOCTYPE html>
<html>

<head>
    <title>Edit Employee</title>
</head>

<body>
    <h1>Edit Employee</h1>

    <form method="POST" action="update_ex8.php" autocomplete="off">
        <input type="hidden" name="id" value="<?php echo $id; ?>">
        <input type="text" name="first_name" placeholder="First Name" value="<?php echo $first_name; ?>" required>
        <input type="text" name="last_name" placeholder="Last Name" value="<?php echo $last_name; ?>" required>
        <input type="text" name="middle_name" placeholder="Middle Name" value="<?php echo $middle_name; ?>" required>
        <input type="text" name="birthday" placeholder="Birthday" value="<?php echo $birthday; ?>" required>
        <input type="text" name="address" placeholder="Address" value="<?php echo $address; ?>" required>
        <button type="submit">Update</button>
    </form>
</body>

</html>