<?php
class API
{
    function func1($fullName)
    {
        echo "Full name: " . $fullName . "<br>";
    }

    function func2($hobbies)
    {
        echo "Hobbies: " . "<br>&nbsp;&nbsp;&nbsp;";
        echo implode("<br>&nbsp;&nbsp;&nbsp;", $hobbies);
        echo "<br>";
    }

    function func3($otherinfo)
    {
        echo "Age: " . $otherinfo['age'] . "<br>";
        echo "Email: " . $otherinfo['email'] . "<br>";
        echo "Birthday: " . $otherinfo['birthday'] . "<br>";
    }
}

$myinfo = new API();

$myinfo->func1("Orlann S. Argarin");

$myhobbies = ["Playing computer games", "Playing bass guitar", "Listening to music"];
$myinfo->func2($myhobbies);

$otherinfo = [
    'age' => 21,
    'email' => 'orlannargarin@gmail.com',
    'birthday' => 'February 2, 2002'
];

$myinfo->func3($otherinfo);
?>