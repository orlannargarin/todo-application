<?php
$host = 'localhost';
$username = 'root';
$password = '';
$database = 'p8_exercise_backend';

$conn = new mysqli($host, $username, $password, $database);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    $id = $_POST["id"];
    $first_name = $_POST["first_name"];
    $last_name = $_POST["last_name"];
    $middle_name = $_POST["middle_name"];
    $birthday = $_POST["birthday"];
    $address = $_POST["address"];

    // Update the employee in the database
    $sql = "UPDATE employee SET first_name=?, last_name=?, middle_name=?, birthday=?, address=? WHERE id=?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("sssssi", $first_name, $last_name, $middle_name, $birthday, $address, $id);

    if ($stmt->execute()) {
        header("Location: exercise7.php"); // Redirect to the employee list page
    } else {
        echo "Error updating employee: " . $stmt->error;
    }
} else {
    echo "Invalid request.";
}
?>