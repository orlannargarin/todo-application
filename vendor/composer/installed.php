<?php return array(
    'root' => array(
        'name' => '__root__',
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'reference' => 'd113e85ebbda438edc57462b8f9a298f553282e4',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => 'd113e85ebbda438edc57462b8f9a298f553282e4',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'thingengineer/mysqli-database-class' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => '5159467ae081adbe96586e45e65a58c0fe7a32ce',
            'type' => 'library',
            'install_path' => __DIR__ . '/../thingengineer/mysqli-database-class',
            'aliases' => array(
                0 => '9999999-dev',
            ),
            'dev_requirement' => false,
        ),
    ),
);
