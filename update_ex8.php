<?php
require_once('C:\xampp\htdocs\Internship\todo-application\vendor\thingengineer\mysqli-database-class\MysqliDb.php');
$host = 'localhost';
$username = 'root';
$password = '';
$database = 'p8_exercise_backend';

$db = new MysqliDb($host, $username, $password, $database);

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    $id = $_POST["id"];
    $first_name = $_POST["first_name"];
    $last_name = $_POST["last_name"];
    $middle_name = $_POST["middle_name"];
    $birthday = $_POST["birthday"];
    $address = $_POST["address"];

    // Define the data to be updated
    $data = array(
        "first_name" => $first_name,
        "last_name" => $last_name,
        "middle_name" => $middle_name,
        "birthday" => $birthday,
        "address" => $address
    );

    // Specify the condition for the update
    $db->where('id', $id);

    // Update the employee in the 'employee' table
    if ($db->update('employee', $data)) {
        header("Location: exercise8.php"); // Redirect to the employee list page
    } else {
        echo "Error updating employee: " . $db->getLastError();
    }

    // Close the database connection
    $db->disconnect();
} else {
    echo "Invalid request.";
}
?>