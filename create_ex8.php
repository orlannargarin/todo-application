<?php
require_once('C:\xampp\htdocs\Internship\todo-application\vendor\thingengineer\mysqli-database-class\MysqliDb.php');
$host = 'localhost';
$username = 'root';
$password = '';
$database = 'p8_exercise_backend';

$db = new MysqliDb($host, $username, $password, $database);

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    $first_name = $_POST["first_name"];
    $last_name = $_POST["last_name"];
    $middle_name = $_POST["middle_name"];
    $birthday = $_POST["birthday"];
    $address = $_POST["address"];

    // Define the data to be inserted
    $data = array(
        "first_name" => $first_name,
        "last_name" => $last_name,
        "middle_name" => $middle_name,
        "birthday" => $birthday,
        "address" => $address
    );

    // Insert the data into the 'employee' table
    $id = $db->insert('employee', $data);

    if ($id) {
        header("Location: exercise8.php"); // Refresh page
    } else {
        echo "Error creating entry: " . $db->getLastError();
    }

    $db->disconnect();
} else {
    echo "Invalid request.";
}
?>