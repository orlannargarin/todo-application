<?php
require_once('C:\xampp\htdocs\Internship\todo-application\vendor\thingengineer\mysqli-database-class\MysqliDb.php');
$host = 'localhost';
$username = 'root';
$password = '';
$database = 'p8_exercise_backend';

$db = new MysqliDb($host, $username, $password, $database);

if ($_SERVER["REQUEST_METHOD"] === "GET" && isset($_GET["id"])) {
    $id = $_GET["id"];

    // Specify the condition for the deletion
    $db->where('id', $id);

    // Delete the employee from the 'employee' table
    if ($db->delete('employee')) {
        header("Location: exercise8.php"); // Redirect to the employee list page
    } else {
        echo "Error deleting employee: " . $db->getLastError();
    }

    // Close the database connection
    $db->disconnect();
} else {
    echo "Invalid request.";
}

?>