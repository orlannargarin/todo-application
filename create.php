<?php
$host = 'localhost';
$username = 'root';
$password = '';
$database = 'p8_exercise_backend';

$conn = new mysqli($host, $username, $password, $database);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    $first_name = $_POST["first_name"];
    $last_name = $_POST["last_name"];
    $middle_name = $_POST["middle_name"];
    $birthday = $_POST["birthday"];
    $address = $_POST["address"];

    // Insert the new employee into the database
    $sql = "INSERT INTO employee (first_name, last_name, middle_name, birthday, address) VALUES (?, ?, ?, ?, ?)";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("sssss", $first_name, $last_name, $middle_name, $birthday, $address);

    if ($stmt->execute()) {
        header("Location: exercise7.php"); // refresh page
    } else {
        echo "Error creating entry: " . $stmt->error;
    }
} else {
    echo "Invalid request.";
}
?>