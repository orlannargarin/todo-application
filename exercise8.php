<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        body {
            font-family: Inter;
        }

        .error {
            color: #FF0000;
        }

        .edit {
            margin-right: 50px;
            text-decoration: none;
        }

        .del {
            margin-right: 50px;
            text-decoration: none;
        }
    </style>
    <title>Employee Database</title>
</head>

<body>
    <?php
    require_once('C:\xampp\htdocs\Internship\todo-application\vendor\thingengineer\mysqli-database-class\MysqliDb.php');
    $host = 'localhost';
    $username = 'root';
    $password = '';
    $database = 'p8_exercise_backend';

    $db = new MysqliDb($host, $username, $password, $database);
    ?>

    <h1>Employee Database</h1>
    <div class="reg"> <!-- create -->
        <h2>Register Employee</h2>
        <form method="POST" action="create_ex8.php" autocomplete="off">
            <label for="first_name">First Name:</label>
            <input type="text" id="first_name" name="first_name" required>
            <br><br>
            <label for="last_name">Last Name:</label>
            <input type="text" id="last_name" name="last_name" required>
            <br><br>
            <label for="middle_name">Middle Name:</label>
            <input type="text" id="middle_name" name="middle_name" required>
            <br><br>
            <label for="birthday">Birthday: </label>
            <input type="text" id="brithday" name="birthday" required>
            <br><br>
            <label for="address">Address: </label>
            <input type="text" id="address" name="address" required>
            <br><br>
            <button type="submit">Submit</button>
        </form>
    </div>
    <br>
    <div class="search"> <!-- read -->
        <h2>Display Employees</h2>
        <table border="1">
            <tr>
                <th>ID</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Middle Name</th>
                <th>Birthday</th>
                <th>Address</th>
                <th>Options</th>
            </tr>
            <?php
            // Retrieve and display employees
            $users = $db->get('employee');

            if ($db->count > 0) {
                foreach ($users as $row) {
                    echo "<tr>";
                    echo "<td>" . $row['id'] . "</td>";
                    echo "<td>" . $row['first_name'] . "</td>";
                    echo "<td>" . $row['last_name'] . "</td>";
                    echo "<td>" . $row['middle_name'] . "</td>";
                    echo "<td>" . $row['birthday'] . "</td>";
                    echo "<td>" . $row['address'] . "</td>";
                    echo "<td><a href='edit_ex8.php?id=" . $row['id'] . "'>Edit</a> | <a href='delete_ex8.php?id=" . $row['id'] . "'>Delete</a></td>";
                    echo "</tr>";
                }
            } else {
                echo "<tr><td colspan='6'>No employee found.</td></tr>";
            }
            ?>
        </table>
    </div>
</body>

<?php
$db->disconnect();
?>

</html>